# zakk-example

Familiarize yourself with the structure of `package.json` and `webpack.config.js`

Clone the repo with

`git clone https://bitbucket.org/raypulver/zakk-example`

`cd` to the repo, then install the dependencies with

`npm install`

Then you can build with

`webpack`

Or you can run the hot reloading server with

`npm start` 
