"use strict";

import { connect } from 'react-redux';
import Application from '../components/Application.jsx';

module.exports = connect((state) => {
  return {
    isTicking: state.tickActive,
    tickIndex: state.tickIndex
  };
}, (dispatch) => {
  return {
    onClick() {
      return dispatch({
        type: 'TOGGLE_TICK'
      });
    },
    incrementTick() {
      return dispatch({
        type: 'INCREMENT_TICK'
      });
    }
  };
})(Application);
