"use strict";

import React from 'react';
import classnames from 'classnames';

const colors = ['red', 'blue', 'green'];

const chooseColor = (colorIndex) => {
  return colors[colorIndex % colors.length];
};

module.exports = ({ onClick, tickIndex, incrementTick, isTicking }) => {
  setTimeout(() => {
    if (isTicking) incrementTick();
  }, 1000);
  return <div className={ classnames({
    'dynamic-div': true,
    'dynamic-off': !isTicking,
    'dynamic-on': isTicking
  }) } onClick={ onClick } style={ { color: chooseColor(tickIndex) } }>we really out here</div>;
};
