"use strict";

import React from 'react';
import { render } from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import ApplicationContainer from './containers/ApplicationContainer.jsx';

const store = createStore(combineReducers({
  tickActive: (state = true, action) => {
    switch (action.type) {
      case 'TOGGLE_TICK':
        return Boolean(state ^ 1);
    }
    return state;
  },
  tickIndex: (state = 0, action) => {
    switch (action.type) {
      case 'INCREMENT_TICK':
        return state + 1;
    }
    return state;
  }
}));

window.store = store;


render(
  <Provider store={store}>
    <ApplicationContainer />
  </Provider>,
  document.getElementById('app')
);
    

