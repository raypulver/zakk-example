"use strict";

var join = require('path').join;

module.exports = {
  entry: [
    'webpack-dev-server/client?http://0.0.0.0:3000',
    'webpack/hot/only-dev-server',
    join(__dirname, 'assets', 'main.js')
  ],
  output: {
    path: join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'stage-2', 'react', 'react-hmre']
      }
    }, {
      test: /\.less$/,
      loader: 'style!css!less'
    }]
  }
};
