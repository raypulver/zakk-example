var express,
    app = (express = require('express'))(),
    join = require('path').join;

app.use(express.static(join(__dirname, 'public'))).listen(4000);
